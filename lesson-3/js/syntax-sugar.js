const sheep = ["🐑", "🐑", "🐑", "🐑"];
const fakeSheep = sheep;
const cloneSheep1 = sheep.slice();
const cloneSheep2 = [...sheep]; // Spread Operator

console.log(sheep);
console.log(fakeSheep);

fakeSheep.push("🐷");

log("sheep", sheep);
log("fake", fakeSheep);
log("clone1", cloneSheep1);
log("clone2", cloneSheep2);

const hero = {
    alias: "Captain Obvious",
    realName: "Bruce Peter",
    superpower: "Stating the obvious",
}

const hero2 = {
    ...hero,
    alignment: "Evil",
    alias: "Caption Not So Obvious"
};






function log(label, value) {
  console.log(label);
  console.log(value);
}
