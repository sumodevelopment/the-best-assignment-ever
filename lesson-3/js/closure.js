function init(name) {
    // OUTER SCOPE
    return function() {
        // INNER SCOPE
        console.log(name);
    }
}
const printName = init("dewald");
printName();

// Website with (12, 16, 18, 22)
function makeSizer(size) {
    return function() {
        document.body.style.fontSize = size + "px";
    }
}

const size12 = makeSizer(12);
const size16 = makeSizer(16);
const size18 = makeSizer(18);
const size22 = makeSizer(22);

document.getElementById("button-12").onclick = size12;




